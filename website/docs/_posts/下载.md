---
title: 下载
permalink: /download
navbar: true
sidebar: false
prev: false
next: false
search: false
article: false
comment: false
editLink: false
date: 2021-10-23 01:45:12
---
## 从代码仓库获取

### 适用场景

-   基于OpenHarmony的稳定分支建立自己的基线，分发下游客户。
-   已经完成自身软件与OpenHarmony的对接，需要进行OpenHarmony官方认证。
-   芯片/模组/app通过OpenHarmony官方认证后，贡献代码到OpenHarmony社区。
-   修复OpenHarmony的问题。
-   学习OpenHarmony的源码。

### 准备

1. 注册码云gitee账号。

2. 注册码云SSH公钥，请参考<a href="https://gitee.com/help/articles/4181" target="_blank">码云帮助中心</a>。

3. 安装<a href="http://git-scm.com/book/zh/v2/%E8%B5%B7%E6%AD%A5-%E5%AE%89%E8%A3%85-Git" target="_blank">git客户端</a>和<a href="https://gitee.com/vcs-all-in-one/git-lfs?_from=gitee_search#downloading"  target="_blank">git-lfs</a>并配置用户信息。

   ```
   git config --global user.name "yourname"
   git config --global user.email "your-email-address"
   git config --global credential.helper store
   ```

4. 安装码云repo工具，可以执行如下命令。

   ```
   curl https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 > /usr/local/bin/repo
   chmod a+x /usr/local/bin/repo
   pip install -i https://repo.huaweicloud.com/repository/pypi/simple requests
   ```

### 操作

**获取轻量系统/小型系统源码**

><img src="/images/zh-cn/device-dev/public_sys-resources/icon-note.gif"> **说明：** 
>主干代码为开发分支，开发者可通过主干代码获取最新特性。release分支代码相对比较稳定，开发者可基于release分支代码进行商用功能开发。

- **OpenHarmony主干代码获取**

  方式一（推荐）：通过repo下载

  ```
  repo init -u https://gitee.com/openharmony/manifest.git -b master --no-repo-verify
  repo sync -c
  repo forall -c 'git lfs pull'
  ```

  方式二：通过git clone单个代码仓库

  进入<a href="https://gitee.com/openharmony"  target="_blank">代码仓库主页</a>，选择需要克隆的代码仓库，执行命令，如：

  ```
  git clone https://gitee.com/openharmony/manifest.git -b master
  ```


- **OpenHarmony release 分支代码获取**

  通过repo下载

  ```
  repo init -u https://gitee.com/openharmony/manifest.git  -b OpenHarmony_1.0.1_release --no-repo-verify
  repo sync -c  
  repo forall -c 'git lfs pull'
  ```

- **OpenHarmony**  其他版本源码获取方式请参考版本[Release-Notes](/download)。

**获取标准系统源码（2.0 Canary）**

1. 创建OpenHarmony工作目录。

   ```
   mkdir OpenHarmony
   ```

2. 切换到OpenHarmony工作目录。

   ```
   cd OpenHarmony
   ```

3. repo初始化。

   ```
   repo init -u https://gitee.com/openharmony/manifest.git -b master --no-repo-verify
   ```

4. 更新代码。

   ```
   repo sync -c
   ```

5. 更新二进制。

   ```
   repo forall -c 'git lfs pull'
   ```

## 从镜像站点获取

为了获得更好的下载性能，您可以选择从以下站点的镜像库获取源码或者对应的解决方案。

><img src="/images/zh-cn/device-dev/public_sys-resources/icon-note.gif"> **说明：** 
>
>-   本部分只提供**OpenHarmony**  Master最新版本和LTS最新版本的源码获取方式， 其他版本源码获取方式以及具体版本信息请参考[Release-Notes](/downloads)
>-   当前Master 1.0版本已经不再维护。

**表 1**  源码获取路径

| LTS版本源码              | 版本信息     | 下载站点                                                                                           | SHA256校验码                                                                                                              |
| ------------------------ | ------------ | -------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------- |
| 全量代码                 | 1.1.0        | [站点](https://repo.huaweicloud.com/harmonyos/os/1.1.0/code-1.1.0.tar.gz "站点")                   | [SHA256 校验码](https://repo.huaweicloud.com/harmonyos/os/1.1.0/code-1.1.0.tar.gz.sha256 "SHA256 校验码")                 |
| Hi3861解决方案（二进制） | 1.1.0        | [站点](https://repo.huaweicloud.com/harmonyos/os/1.1.0/wifiiot-1.1.0.tar.gz "站点")                | [SHA256 校验码](https://repo.huaweicloud.com/harmonyos/os/1.1.0/code-1.1.0.tar.gz.sha256 "SHA256 校验码")                 |
| Hi3518解决方案（二进制） | 1.1.0        | [ 站点](https://repo.huaweicloud.com/harmonyos/os/1.1.0/ipcamera_hi3518ev300-1.1.0.tar.gz " 站点") | [SHA256 校验码](https://repo.huaweicloud.com/harmonyos/os/1.1.0/ipcamera_hi3518ev300-1.1.0.tar.gz.sha256 "SHA256 校验码") |
| Hi3516解决方案（二进制） | 1.1.0        | [站点](https://repo.huaweicloud.com/harmonyos/os/1.1.0/ipcamera_hi3516dv300-1.1.0.tar.gz "站点")   | [SHA256 校验码](https://repo.huaweicloud.com/harmonyos/os/1.1.0/ipcamera_hi3516dv300-1.1.0.tar.gz.sha256 "SHA256 校验码") |
| RELEASE-NOTES            | 1.1.0        | [站点](https://repo.huaweicloud.com/harmonyos/os/1.1.0/OpenHarmony_Release_Notes_zh_cn.zip "站点") |                                                                                                                           |
| **Master版本源码**       | **版本信息** | **下载站点**                                                                                       | **SHA256校验码**                                                                                                          |
| 全量代码                 | 2.0 Canary   | [ 站点](https://repo.huaweicloud.com/harmonyos/os/2.0/code-2.0-canary.tar.gz " 站点")              | [SHA256 校验码](https://repo.huaweicloud.com/harmonyos/os/2.0/code-2.0-canary.tar.gz.sha256 "SHA256 校验码")              |
| **编译工具链**           | **版本信息** | **下载站点**                                                                                       | **SHA256校验码**                                                                                                          |
| 编译工具链获取清单       | 2.0          | <a target="_blank" href="https://repo.huaweicloud.com/harmonyos/os/2.0/tool_chain/">站点</a>       | 2021年6月2日晚发布                                                                                                        |

## 源码目录简介

下表是OpenHarmony源码的目录及简单说明：

**表 2**  源码目录

| 目录名       | 描述                                  |
| ------------ | ------------------------------------- |
| applications | 应用程序样例，包括camera等            |
| base         | 基础软件服务子系统集&硬件服务子系统集 |
| build        | 组件化编译、构建和配置脚本            |
| docs         | 说明文档                              |
| domains      | 增强软件服务子系统集                  |
| drivers      | 驱动子系统                            |
| foundation   | 系统基础能力子系统集                  |
| kernel       | 内核子系统                            |
| prebuilts    | 编译器及工具链子系统                  |
| test         | 测试子系统                            |
| third_party  | 开源第三方组件                        |
| utils        | 常用的工具集                          |
| vendor       | 厂商提供的软件                        |
| build.py     | 编译脚本文件                          |

