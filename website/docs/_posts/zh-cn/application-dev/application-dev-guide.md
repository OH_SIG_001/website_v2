---
title: application-dev-guide
permalink: /pages/extra/e59705/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:36
---
# 应用开发导读

应用开发文档用于指导开发者通过OpenHarmony提供的接口完成应用开发。当前应用开发文档提供了在标准系统上开发应用的JS接口。

在这部分中，开发者可以通过“[入门](/pages/extra/82bbc8/)”来了解应用开发的基本方法。完整的接口清单和参考使用指导可参见“[开发参考](/pages/extra/23166e/)”。

除此之外，为方便开发者对常用功能进行深入理解，还提供了[UI](/pages/extra/91bbde/)、[媒体](/pages/extra/504ad4/)、[网络与连接](/pages/extra/30f113/)三个模块的开发指南。

如果需要了解各子系统的原理和基本信息，可以参考“docs/zh-cn/readme”目录中各子系统readme的介绍。

