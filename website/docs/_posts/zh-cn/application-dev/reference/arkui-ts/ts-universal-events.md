---
title: ts-universal-events
permalink: /pages/extra/1ab473/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 通用事件<a name="ZH-CN_TOPIC_0000001237715071"></a>

-   **[点击事件](/pages/010c020201010101)**  

-   **[触摸事件](/pages/010c020201010102)**  

-   **[挂载卸载事件](/pages/010c020201010103)**  

-   **[按键事件](/pages/010c020201010104)**  

-   **[组件区域变化事件](/pages/010c020201010105)**  


