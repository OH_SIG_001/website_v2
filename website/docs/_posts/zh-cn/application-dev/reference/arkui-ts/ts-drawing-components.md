---
title: ts-drawing-components
permalink: /pages/extra/0ed70d/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 绘制组件<a name="ZH-CN_TOPIC_0000001192915088"></a>

-   **[Circle](/pages/010c0202010401)**  

-   **[Ellipse](/pages/010c0202010402)**  

-   **[Line](/pages/010c0202010403)**  

-   **[Polyline](/pages/010c0202010404)**  

-   **[Polygon](/pages/010c0202010405)**  

-   **[Path](/pages/010c0202010406)**  

-   **[Rect](/pages/010c0202010407)**  

-   **[Shape](/pages/010c0202010408)**  


