---
title: js-svg
permalink: /pages/extra/c15464/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# svg组件<a name="ZH-CN_TOPIC_0000001127284910"></a>

-   **[通用属性](/pages/010c0201010701)**  

-   **[svg](/pages/010c0201010702)**  

-   **[rect](/pages/010c0201010703)**  

-   **[circle](/pages/010c0201010704)**  

-   **[ellipse](/pages/010c0201010705)**  

-   **[path](/pages/010c0201010706)**  

-   **[line](/pages/010c0201010707)**  

-   **[polyline](/pages/010c0201010708)**  

-   **[polygon](/pages/010c0201010709)**  

-   **[text](/pages/010c020101070a)**  

-   **[tspan](/pages/010c020101070b)**  

-   **[textPath](/pages/010c020101070c)**  

-   **[animate](/pages/010c020101070d)**  

-   **[animateMotion](/pages/010c020101070e)**  

-   **[animateTransform](/pages/010c020101070f)**  


