---
title: Readme-CN
permalink: /pages/extra/74333b/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
#  基于JS扩展的类Web开发范式

-   [组件](/pages/extra/075858/)
    -   [通用](/pages/extra/1ce3c8/)
        -   [通用属性](/pages/010c0201010101)
        -   [通用样式](/pages/010c0201010102)
        -   [通用事件](/pages/010c0201010103)
        -   [通用方法](/pages/010c0201010104)
        -   [动画样式](/pages/010c0201010105)
        -   [渐变样式](/pages/010c0201010106)
        -   [转场样式](/pages/010c0201010107)
        -   [媒体查询](/pages/010c0201010108)
        -   [自定义字体样式](/pages/010c0201010109)
        -   [原子布局](/pages/010c020101010a)

    -   [容器组件](/pages/extra/c45b2f/)
        -   [badge](/pages/010c0201010201)
        -   [dialog](/pages/010c0201010202)
        -   [div](/pages/010c0201010203)
        -   [form](/pages/010c0201010204)
        -   [list](/pages/010c0201010205)
        -   [list-item](/pages/010c0201010206)
        -   [list-item-group](/pages/010c0201010207)
        -   [panel](/pages/010c0201010208)
        -   [popup](/pages/010c0201010209)
        -   [refresh](/pages/010c020101020a)
        -   [stack](/pages/010c020101020b)
        -   [stepper](/pages/010c020101020c)
        -   [stepper-item](/pages/010c020101020d)
        -   [swiper](/pages/010c020101020e)
        -   [tabs](/pages/010c020101020f)
        -   [tab-bar](/pages/010c0201010210)
        -   [tab-content](/pages/010c0201010211)

    -   [基础组件](/pages/extra/d6389e/)
        -   [button](/pages/010c0201010301)
        -   [chart](/pages/010c0201010302)
        -   [divider](/pages/010c0201010303)
        -   [image](/pages/010c0201010304)
        -   [image-animator](/pages/010c0201010305)
        -   [input](/pages/010c0201010306)
        -   [label](/pages/010c0201010307)
        -   [marquee](/pages/010c0201010308)
        -   [menu](/pages/010c0201010309)
        -   [option](/pages/010c020101030a)
        -   [picker](/pages/010c020101030b)
        -   [picker-view](/pages/010c020101030c)
        -   [piece](/pages/010c020101030d)
        -   [progress](/pages/010c020101030e)
        -   [qrcode](/pages/010c020101030f)
        -   [rating](/pages/010c0201010310)
        -   [richtext](/pages/010c0201010311)
        -   [search](/pages/010c0201010312)
        -   [select](/pages/010c0201010313)
        -   [slider](/pages/010c0201010314)
        -   [span](/pages/010c0201010315)
        -   [switch](/pages/010c0201010316)
        -   [text](/pages/010c0201010317)
        -   [textarea](/pages/010c0201010318)
        -   [toolbar](/pages/010c0201010319)
        -   [toolbar-item](/pages/010c020101031a)
        -   [toggle](/pages/010c020101031b)

    -   [媒体组件](/pages/extra/8f898c/)
        -   [video](/pages/010c0201010401)

    -   [画布组件](/pages/extra/c2a5d2/)
        -   [canvas组件](/pages/010c0201010501)
        -   [CanvasRenderingContext2D对象](/pages/010c0201010502)
        -   [Image对象](/pages/010c0201010503)
        -   [CanvasGradient对象](/pages/010c0201010504)
        -   [ImageData对象](/pages/010c0201010505)
        -   [Path2D对象](/pages/010c0201010506)
        -   [ImageBitmap对象](/pages/010c0201010507)
        -   [OffscreenCanvas对象](/pages/010c0201010508)
        -   [OffscreenCanvasRenderingContext2D对象](/pages/010c0201010509)

    -   [栅格组件](/pages/extra/6ab25a/)
        -   [基本概念](/pages/010c0201010601)
        -   [grid-container](/pages/010c0201010602)
        -   [grid-row](/pages/010c0201010603)
        -   [grid-col](/pages/010c0201010604)

    -   [svg组件](/pages/extra/c15464/)
        -   [通用属性](/pages/010c0201010701)
        -   [svg](/pages/010c0201010702)
        -   [rect](/pages/010c0201010703)
        -   [circle](/pages/010c0201010704)
        -   [ellipse](/pages/010c0201010705)
        -   [path](/pages/010c0201010706)
        -   [line](/pages/010c0201010707)
        -   [polyline](/pages/010c0201010708)
        -   [polygon](/pages/010c0201010709)
        -   [text](/pages/010c020101070a)
        -   [tspan](/pages/010c020101070b)
        -   [textPath](/pages/010c020101070c)
        -   [animate](/pages/010c020101070d)
        -   [animateMotion](/pages/010c020101070e)
        -   [animateTransform](/pages/010c020101070f)

-   [自定义组件](/pages/extra/e86efb/)
    -   [基本用法](/pages/010c02010201)
    -   [自定义事件](/pages/010c02010202)
    -   [Props](/pages/010c02010203)
    -   [事件参数](/pages/010c02010204)
    -   [slot插槽](/pages/010c02010205)
    -   [生命周期定义](/pages/010c02010206)
    
-   [附录](/pages/extra/98f426/)
    -   [类型说明](/pages/010c02010301)

