---
title: js-components-custom
permalink: /pages/extra/e86efb/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 自定义组件<a name="ZH-CN_TOPIC_0000001173164753"></a>

-   **[基本用法](/pages/010c02010201)**  

-   **[自定义事件](/pages/010c02010202)**  

-   **[Props](/pages/010c02010203)**  

-   **[事件参数](/pages/010c02010204)**  

-   **[slot插槽](/pages/010c02010205)**  

-   **[生命周期定义](/pages/010c02010206)**  


