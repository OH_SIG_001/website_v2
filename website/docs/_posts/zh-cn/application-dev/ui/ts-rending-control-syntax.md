---
title: ts-rending-control-syntax
permalink: /pages/extra/cc10c8/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:36
---
# 渲染控制语法<a name="ZH-CN_TOPIC_0000001110788982"></a>

-   **[条件渲染](/pages/01080203030401)**  

-   **[循环渲染](/pages/01080203030402)**  

-   **[数据懒加载](/pages/01080203030403)**  


