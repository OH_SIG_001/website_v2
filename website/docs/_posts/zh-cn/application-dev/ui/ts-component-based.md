---
title: ts-component-based
permalink: /pages/extra/b1da24/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:36
---
# 组件化<a name="ZH-CN_TOPIC_0000001110948890"></a>

-   **[@Component](/pages/0108020303020301)**  

-   **[@Entry](/pages/0108020303020302)**  

-   **[@Preview](/pages/0108020303020303)**  

-   **[@Builder](/pages/0108020303020304)**  

-   **[@Extend](/pages/0108020303020305)**  

-   **[@CustomDialog](/pages/0108020303020306)**  


