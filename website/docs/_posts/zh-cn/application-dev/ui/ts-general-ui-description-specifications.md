---
title: ts-general-ui-description-specifications
permalink: /pages/extra/52abc2/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:36
---
# 通用UI描述规范<a name="ZH-CN_TOPIC_0000001157388843"></a>

-   **[基本概念](/pages/01080203030201)**  

-   **[声明式UI描述规范](/pages/extra/62d804/)**  

-   **[组件化](/pages/extra/b1da24/)**  


