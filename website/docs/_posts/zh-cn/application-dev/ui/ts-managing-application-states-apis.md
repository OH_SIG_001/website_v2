---
title: ts-managing-application-states-apis
permalink: /pages/extra/9e57dd/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:36
---
# 接口



- **[应用程序的数据存储](/pages/010802030303030101)**

- **[持久化数据管理](/pages/010802030303030102)**

- **[环境变量](/pages/010802030303030103)**