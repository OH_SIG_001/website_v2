---
title: driver-develop
permalink: /pages/extra/6b1378/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 平台驱动开发<a name="ZH-CN_TOPIC_0000001160769576"></a>

-   **[ADC](/pages/0105020201)**  

-   **[GPIO](/pages/0105020202)**  

-   **[HDMI](/pages/0105020203)**  

-   **[I2C](/pages/0105020204)**  

-   **[I3C](/pages/0105020205)**  

-   **[MIPI-CSI](/pages/0105020206)**  

-   **[MIPI-DSI](/pages/0105020207)**  

-   **[MMC](/pages/0105020208)**  

-   **[PWM](/pages/0105020209)**  

-   **[RTC](/pages/010502020a)**  

-   **[SDIO](/pages/010502020b)**  

-   **[SPI](/pages/010502020c)**  

-   **[UART](/pages/010502020d)**  

-   **[WatchDog](/pages/010502020e)**  
