---
title: porting-smallchip
permalink: /pages/extra/362558/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 小型系统芯片移植指导<a name="ZH-CN_TOPIC_0000001132588824"></a>

-   **[移植准备](/pages/extra/f4e07b/)**  

-   **[移植内核](/pages/extra/2858ab/)**  

-   **[驱动移植](/pages/extra/f63f6f/)**  


