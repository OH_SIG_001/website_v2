---
title: porting-smallchip-driver
permalink: /pages/extra/f63f6f/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 驱动移植<a name="ZH-CN_TOPIC_0000001123149616"></a>

-   **[移植概述](/pages/0104020301)**  

-   **[平台驱动移植](/pages/0104020302)**  

-   **[器件驱动移植](/pages/0104020303)**  


