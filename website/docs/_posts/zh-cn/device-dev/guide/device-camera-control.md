---
title: device-camera-control
permalink: /pages/extra/ac3f29/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 屏幕和摄像头控制<a name="ZH-CN_TOPIC_0000001111199426"></a>

-   **[概述](/pages/010701030101)**  

-   **[示例开发](/pages/extra/781d25/)**  

-   **[应用实例](/pages/010701030103)**  


