---
title: subsys-build
permalink: /pages/extra/7014d3/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 编译构建<a name="ZH-CN_TOPIC_0000001111039546"></a>

-   **[轻量和小型系统编译构建指导](/pages/01050301)**  

-   **[标准系统编译构建指导](/pages/01050302)**  


