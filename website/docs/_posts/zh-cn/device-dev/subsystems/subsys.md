---
title: subsys
permalink: /pages/extra/1328dc/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 子系统<a name="ZH-CN_TOPIC_0000001135844124"></a>

-   **[编译构建](/pages/extra/7014d3/)**  
-   **[分布式远程启动](/pages/010504)**  
-   **[图形图像](/pages/extra/7fcbf2/)**  
-   **[媒体](/pages/extra/c169e0/)**  
-   **[数据管理](/pages/extra/c92e1f/)**  
-   **[公共基础](/pages/extra/ce319f/)**  
-   **[AI框架](/pages/extra/ac23c8/)**  
-   **[Sensor服务](/pages/extra/fdd664/)**  
-   **[用户程序框架](/pages/extra/72b946/)**  
-   **[OTA升级](/pages/01050b)**  
-   **[电话服务](/pages/extra/019068/)**
-   **[安全](/pages/extra/2dd473/)**  
-   **[启动恢复](/pages/extra/9238e4/)**  
-   **[测试用例开发指导](/pages/010901)**  
-   **[DFX](/pages/extra/684ab8/)**  
-   **[研发工具链](/pages/extra/5138cc/)**  
-   **[XTS认证用例开发指导](/pages/010a01)**  


