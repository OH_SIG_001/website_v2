---
title: quickstart-standard-running-hi3516
permalink: /pages/extra/d76ae0/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# Hi3516开发板<a name="ZH-CN_TOPIC_0000001188686298"></a>

-   **[创建应用程序](/pages/010202040101)**  

-   **[源码编译](/pages/010202040102)**  

-   **[镜像烧录](/pages/010202040103)**  

-   **[镜像运行](/pages/010202040104)**  


