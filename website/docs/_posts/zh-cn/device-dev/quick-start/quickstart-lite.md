---
title: quickstart-lite
permalink: /pages/extra/fdee28/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 轻量和小型系统入门<a name="ZH-CN_TOPIC_0000001112826850"></a>

-   **[轻量与小型系统入门概述](/pages/01020101)**  

-   **[搭建轻量与小型系统环境](/pages/extra/770961/)**  

-   **[运行“Hello World”](/pages/extra/e5f98d/)**  

-   **[附录](/pages/extra/d372c4/)**  


