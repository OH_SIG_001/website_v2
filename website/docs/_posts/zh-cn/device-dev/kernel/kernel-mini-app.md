---
title: kernel-mini-app
permalink: /pages/extra/318f40/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 附录<a name="ZH-CN_TOPIC_0000001123948061"></a>

-   **[内核编码规范](/pages/010501010501)**  

-   **[基本数据结构](/pages/extra/96d541/)**  

-   **[标准库支持](/pages/extra/ab87f3/)**  


