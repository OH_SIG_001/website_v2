---
title: kernel-mini-basic-ipc-mutex
permalink: /pages/extra/7b78a2/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 互斥锁<a name="ZH-CN_TOPIC_0000001123948099"></a>

-   **[基本概念](/pages/0105010102040201)**  

-   **[开发指导](/pages/0105010102040202)**  


