---
title: kernel-mini-basic-ipc
permalink: /pages/extra/bad8c5/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 内核通信机制<a name="ZH-CN_TOPIC_0000001124573873"></a>

-   **[事件](/pages/extra/e4f01d/)**  

-   **[互斥锁](/pages/extra/7b78a2/)**  

-   **[消息队列](/pages/extra/c06361/)**  

-   **[信号量](/pages/extra/9d3f31/)**  


