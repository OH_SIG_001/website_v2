---
title: kernel-mini-basic-soft
permalink: /pages/extra/079668/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 软件定时器<a name="ZH-CN_TOPIC_0000001123771893"></a>

-   **[基本概念](/pages/01050101020601)**  

-   **[开发指导](/pages/01050101020602)**  


