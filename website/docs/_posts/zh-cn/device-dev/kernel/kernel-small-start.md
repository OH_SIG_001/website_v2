---
title: kernel-small-start
permalink: /pages/extra/f14e14/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 内核启动<a name="ZH-CN_TOPIC_0000001173040439"></a>

-   **[内核态启动](/pages/010501020201)**  

-   **[用户态启动](/pages/010501020202)**  


