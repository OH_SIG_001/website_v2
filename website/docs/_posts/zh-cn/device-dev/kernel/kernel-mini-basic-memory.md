---
title: kernel-mini-basic-memory
permalink: /pages/extra/d62ea8/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 内存管理<a name="ZH-CN_TOPIC_0000001078876454"></a>

-   **[基本概念](/pages/01050101020301)**  

-   **[静态内存](/pages/01050101020302)**  

-   **[动态内存](/pages/01050101020303)**  


