---
title: kernel-mini-basic
permalink: /pages/extra/9163c3/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 基础内核<a name="ZH-CN_TOPIC_0000001123863157"></a>

-   **[中断管理](/pages/extra/2a9327/)**  

-   **[任务管理](/pages/extra/9ad148/)**  

-   **[内存管理](/pages/extra/d62ea8/)**  

-   **[内核通信机制](/pages/extra/bad8c5/)**  

-   **[时间管理](/pages/extra/726813/)**  

-   **[软件定时器](/pages/extra/079668/)**  


