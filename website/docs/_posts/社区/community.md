---
title: 社区导航
permalink: /community
sidebar: false
article: false
comment: false
editLink: false
date: 2021-07-23 12:13:42
---
# OpenHarmony Community
欢迎来到OpenHarmony社区！

## 介绍
Community仓库用于管理OpenHarmony社区治理、开发者贡献指南、开发者贡献协议、社区交流等内容。

- 社区治理组织架构
    - [项目管理委员会](/community/pmc/)
    - [OpenHarmony社区Committers列表](/community/committers/)
- [开发者社区贡献指南](/community/contribution/)
- [社区交流](/pages/c8d8e3/)

## 社区治理组织架构

OpenHarmony社区通过[项目管理委员会](/community/pmc/)（ Project Management Committee）管理OpenHarmony社区。

## 开发者社区贡献指南

请阅读[如何贡献](/community/contribution/)获得帮助。

## 签署开发者原创声明

您必须首先签署“开发者原创声明”，然后才能参与社区贡献。

点击[这里](https://dco.openharmony.io/sign/Z2l0ZWUlMkZvcGVuX2hhcm1vbnk=)签署、查看签署状态。

## 社区交流

### OpenHarmony Mentors
::: cardList 3
```yaml
- img: /img/OpenHarmonylogo.svg
  link: mailto:macin.liu@huawei.com
  #link: /mentors/liuguo
  name: 刘果
  desc: OpenHarmony开源社区导师

- img: /img/OpenHarmonylogo.svg
  link: mailto:xzmu@openatom.org
  #link: /mentors/liangkelei
  name: 梁克雷
  desc: OpenHarmony 社区技术负责人

- img: /img/OpenHarmonylogo.svg
  link: mailto:qigang@iscas.ac.cn
  #link: /mentors/liangkelei
  name: 朱其罡
  desc: OpenHarmony 生态总负责人

- img: /img/OpenHarmonylogo.svg
  link: mailto:likai20@iscas.ac.cn
  #link: /mentors/liangkelei
  name: 李凯
  desc: SIG组导师

- img: /img/OpenHarmonylogo.svg
  link: mailto:longjun@iscas.ac.cn
  #link: /mentors/liangkelei
  name: 房隆军
  desc: SIG组导师
```
:::

### OpenHarmony maillist 交流方式
| 地址                                 | 简介        | 用途说明                                                         |
| ---------------------------------------|---------- | ------------------------------------------------------------ |
| contact@openharmony.io <img width=150/>  | 公用邮箱 <img width=100/> | OpenHarmony社区公共邮箱。<img width=200/>|
| dev@openharmony.io  <img width=150/>| 开发邮件列表 <img width=100/> | OpenHarmony社区开发讨论邮件列表，任何社区开发相关话题都可以在邮件列表讨论。任何开发者可[订阅](https://lists.openatom.io/postorius/lists/dev.openharmony.io)。<img width=200/>|
| cicd@openharmony.io <img width=150/> | CI邮件列表  <img width=100/>| OpenHarmomny CICD构建邮件列表，任何开发者可[订阅](https://lists.openatom.io/postorius/lists/cicd.openharmony.io)。<img width=200/>|
| pmc@openharmony.io  <img width=150/>| PMC邮件列表  <img width=100/>| PMC讨论邮件列表，PMC成员可[订阅](https://lists.openatom.io/postorius/lists/pmc.openharmony.io/)。<img width=200/>|
| scy@openharmony.io <img width=150/> | 安全问题邮箱 <img width=100/> | 开发者可反馈OpenHarmony安全问题到此邮箱。<img width=200/>|
| scy-priv@openharmony.io  <img width=150/>| 安全组邮件列表  <img width=100/>| 安全组成员安全问题处理讨论邮件列表，安全组成员可[订阅](https://lists.openatom.io/postorius/lists/scy-priv.openharmony.io/)。<img width=200/>|


### OpenHarmony Slack 交流频道
| 技术交流平台                                 | 简介        | 用途说明                                                         |
| ---------------------------------------|---------- | ------------------------------------------------------------ |
| [Slack](https://join.slack.com/t/openharmonyworkspace/shared_invite/zt-qupqqon4-ySvYnCJTJfBrHsNsGNFYEA) <img width=150/>  | OpenHarmony 技术开放讨论平台 <img width=100/> | OpenHarmony Slack 中不同频道用于相关技术问题交流和讨论，当前有kernel、AI、devboard、qemu、risc-v等专属频道，Slack工具[下载地址](https://gitee.com/dongjinguang/free-tools/tree/master/slack)。<img width=200/>|


## 项目LOGO

请从[OpenHarmony LOGO](/community/maillist/)下载并使用LOGO。
