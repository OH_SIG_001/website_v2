---
title: Raspberrypi_Model_3b+
permalink: /supported_devices/RPI_3B/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: false
comment: false
editLink: false
date: 2021-10-11 17:56:59
---

# Raspberrypi_Model_3b+

<img src="/devices/RPI_3B/figures/raspberrypi_Model_3b+/front.png">

<img src="/devices/RPI_3B/figures/raspberrypi_Model_3b+/back.png">

# 开发板介绍

Raspberry Pi 3 model B+ 开发板是由“Raspberry Pi 慈善基金会”开发，基于BCM2837B0芯片，

目的是以低价硬件及自由软件促进学校的基本计算机科学教育。

# 芯片参数

| 名称 | 参数                                                         |
| ---- | ------------------------------------------------------------ |
| Soc  | Broadcom BCM2837B0                                           |
| CPU  | Quad-core，Cortex-A53 (ARMv8) 64-bit SoC @ 1.4GHz            |
| GPU  | 400MHz的IV的VideoCore GPU                                    |
| RAM  | 1GB LPDDR2 SDRAM                                             |
| ROM  | Micro SD port for loading your operating system and storing data |



# 外设

| 名称          | 参数                                                         |
| ------------- | ------------------------------------------------------------ |
| Ethernet Port | Gigabit Ethernet over USB 2.0 (maximum throughput 300 Mbps)  |
| Wireless      | WiFi: 5 GHz & 2.4GHz IEEE 802.11 a/b/g/n/ac <br> Bluetooth® v4.2<br> Internal Antenna |
| USB           | 4路USB 2.0 ports                                             |
| Display       | Full-size HDMI                                               |
| Media         | 4 极点立体声输出和复合视频端口                               |
| UART          | 1路                                                          |
| I2C           | 1路                                                          |
| SP            | 1路                                                          |
| GPIO          | 40路                                                         |
| Power Source  | 5V/2.5A DC MicroUSB 电源输入接口                             |



# 其他

* OpenHarmony 兼容性认证（进行中）
* [Software](https://gitee.com/openharmony-sig/devboard_vendor_rpi3b)                             
* [Hardware](https://www.raspberrypi.org/documentation/computers/raspberry-pi.html/)
* [Support](https://zulip.openharmony.cn/#narrow/stream/7-devboard_sig/topic/Raspberrypi_Porting)
