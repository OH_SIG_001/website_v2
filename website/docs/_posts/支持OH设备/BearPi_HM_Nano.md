---
title: 小熊派BearPi-HM Nano
permalink: /supported_devices/BearPi_HM_Nano
navbar: true
sidebar: false
prev: false
next: false
search: false
article: false
comment: false
editLink: false
date: 2021-10-11 17:56:59
---
# 小熊派BearPi-HM Nano

<img src="/devices/BearPi_HM_Nano/figures/BearPi_HM_Nano/BearPi-HM_NanoBoardDetail.png">

# 芯片参数
|  名称   | 参数  |
|  ----  | ----  |
|  Soc  | Hi3861  |
|  CPU  | 高性能 32 bit 微处理器，最大工作频率 160MHz  |
|  RAM  | 内嵌 SRAM 352KB |
|  ROM  | 288KB |
| Flash | 内嵌2MB Flash |

# 外设
|  名称   | 参数  |
|  ----  | ----  |
|  WiFi  |  2.4GHz IEEE 802.11 a/b/g/n/ac  |
|  Wireless  | 3路 |
|  UART  | 1路 |
|  I2C  | 1路 |
|  SPI  | 1路 |
|  GPIO  | 15路 |
|  ADC  | 1路 |
|Power Source |DC 5v|

# 其他
* OpenHarmony 兼容性认证（通过）
* [Software](https://gitee.com/openharmony/device_bearpi_bearpi_hm_nano)                             
* [Hardware](https://gitee.com/bearpi/bearpi-hm_nano/tree/master/applications/BearPi/BearPi-HM_Nano/docs/board)
* [Support](https://gitee.com/openharmony/device_bearpi_bearpi_hm_nano/issues)
