---
title: BK7231X
permalink: /supported_devices/BK7231X
navbar: true
sidebar: false
prev: false
next: false
search: false
article: false
comment: false
editLink: false
date: 2021-10-11 17:56:59
---




# BK7231X


<img src="/devices/BK7231X/figures/front.jpg">
<img src="/devices/BK7231X/figures/back.jpg">



# 芯片参数

| 名称 | 参数            |
| ---- | --------------- |
| Soc  | BK7231M/BL2028N |
| CPU  | ARM9 120MHz     |
| RAM  | 256 Kbyte       |



# 外设

| 名称         | 参数                                                         |
| ------------ | ------------------------------------------------------------ |
| Wireless     | WiFi: 2.4GHz IEEE 802.11 b/g/n/<br/>Bluetooth® v5.2<br/> External Antenna |
| UART         | 2路                                                          |
| I2C          | 2路                                                          |
| SPI          | 1路                                                          |
| GPIO         | 19路                                                         |
| SDIO         | 1路                                                          |
| Power Source | DC 2.8V - 3.6V                                               |



# 其他

* OpenHarmony 兼容性认证（进行中）
* [Software](https://gitee.com/openharmony-sig/device_beken )                            
* [Hardware](https://gitee.com/xxkit/bk7231_for_hilink) 
* [Support](https://gitee.com/openharmony-sig/device_beken/issues) 
