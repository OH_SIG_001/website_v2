---
title: XR806
permalink: /supported_devices/XR806
navbar: true
sidebar: false
prev: false
next: false
search: false
article: false
comment: false
editLink: false
date: 2021-10-11 17:56:59
---
# XR806

<img src="/devices/XR806/figures/front.png">

<img src="/devices/XR806/figures/back.png">

# 芯片参数

| 名称   | 参数                                                         |
| ------ | ------------------------------------------------------------ |
| CPU    | Arm-Star ARMv8-M MCU, up to 160MHz                           |
| DDR    | 288KB SRAM                                                   |
| Memory | 160KB Code ROM. SIP  16Mbit Flash                            |
| eFuse  | 1024 bits. It can be uesd to write WiFi/BT MAC address, Chip ID,security KEY and so on. |



# 外设

| 名称       | 参数                                                         |
| ---------- | ------------------------------------------------------------ |
| WiFi       | Compatible with 802.11b/g/n standard <br> Single-band 2.4G 1T1R WLAN  <br>WPA/WPA2.0 personal/WPA3.0 personal/ WPS2.0 <br/> Integrated LNA PA and T/R switch  <br/>STA, AP, STA/AP and Monitor mode  <br/>Supports Wi-Fi and Bluetooth Co-existenc |
| BLE        | Bluetooth Low Energy V5.0 1Mbps, 2Mbps and long range <br/>-97dBm RX sensitivity in 1Mbps mode<br/> -103dBm RX sensitivity in 125kbps mode<br/> TX power: -20dBm ~ 20dBm |
| Audio      | 1路                                                          |
| irDA TX/RX | 1路                                                          |
| RTC        | 1路                                                          |
| UART       | 3路                                                          |
| I2C        | 2路                                                          |
| SPI        | 1路                                                          |
| GPIO       | 14路                                                         |
| PWM        | 8路                                                          |
| ADC        | 8路                                                          |
| GP Timer   | 3路                                                          |
| Power      | 1.62 ~ 5.5V single power supply integrated with DC-DC<br/> Shutdown: 0.5uA<br/> Hibernation: 5uA@RTC only<br/> Standby: 28uA@16KB SRAM retention<br/> WLAN DTIM interval with 32.768kHz XTAL:580uA@DTIM1, 230uA@DTIM3, 90uA@DTIM10<br/> WLAN RX active: 20mA@11b 1M Beacon<br/> WLAN TX active: 180mA@16dBm, 85mA@10dBm |



# 其他
* OpenHarmony 兼容性认证（通过）
* [Software](https://gitee.com/openharmony-sig/devboard_device_allwinner_xr806)   
* [Hardware](https://xr806.docs.aw-ol.com/)                          
* [Support](https://gitee.com/openharmony-sig/devboard_device_allwinner_xr806/issues)
