---
title: Halley5
permalink: /supported_devices/Halley5
navbar: true
sidebar: false
prev: false
next: false
search: false
article: false
comment: false
editLink: false
date: 2021-10-11 17:56:59
---

# Halley5

<img src="/devices/Halley5/figures/INGENIC-HALLEY5/halley5-TOP.png">
<img src="/devices/Halley5/figures/INGENIC-HALLEY5/halley5-BOTTOM.png">
<img src="/devices/Halley5/figures/INGENIC-HALLEY5/halley5-CORE.png">

# 芯片参数

| 名称 | 参数                                                         |
| ---- | ------------------------------------------------------------ |
| Soc  | X2000                                                        |
| CPU  | 2 x Xburst2® (mips based) up to 1.5 GHz + Xburst0®(mips based)240MHz |
| VPU  | H.264 En/Decoder                                             |
| ISP  | 2路  up to 1080P/60fps                                       |
| RAM  | Siped 128Mbyte LPDDR3(256Mbyte/512Mbyte option, pin to pin compatible) |
| ROM  | QSPI x 2<br/>eMMC5.1 x 2<br/>SDIO3.0 x 3                     |



# 外设

| 名称     | 参数                                                        |
| -------- | ----------------------------------------------------------- |
| Ethernet | 2 x 10/100/1000 Mbit/s, IEEE 802.3/IEEE 1588-2002 Compliant |
| USB      | OTG: 1 x type C, 2.0 high-speed                             |
| Display  | MIPI DSI2 1.0/RGB888                                        |
| Camera   | MIPI CSI x 2 + DVP                                          |
| Audio    | CodecAD/DA 24bit<br/> S/PDIF(IN/OUT)                        |
| SADC     | 6路                                                         |
| PWM      | 16路                                                        |
| UART     | 10路                                                        |
| I2C      | 6路                                                         |
| SPI      | 2路                                                         |
| GPIO     | 118路                                                       |
| SDIO     | 3路                                                         |
| I2S      | 3路                                                         |
| DMIC     | 4路                                                         |
| Power    | DC   5V   <400mW                                            |



# 其他

* OpenHarmony 兼容性认证（进行中）
* [Software](https://gitee.com/openharmony-sig/device_ingenic)
* [Hardware](https://pan.baidu.com/s/1PxHJhv7j_oXkFTjAVNInxA#list/path=%2F)    提取码： 6svw
* [Support](https://gitee.com/openharmony-sig/device_ingenic/issues)

