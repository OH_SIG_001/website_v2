---
home: true
# heroImage: /img/web.png
heroText: OpenHarmony
tagline: 万物互联 汇聚如一
#actions:
#  - text: 快速上手
#    link: /guide/documents/
#    type: primary
#  - text: 项目简介
#    link: /guide/documents/
#    type: secondary
actionText: 快速入门
actionLink: /pages/0000
# bannerBg: auto # auto => 网格纹背景(有bodyBgImg时无背景)，默认 | none => 无 | '大图地址' | background: 自定义背景样式       提示：如发现文本颜色不适应你的背景时可以到palette.styl修改$bannerTextColor变量
bannerBg: '/images/banner00.png'

# 文章列表显示方式: detailed 默认，显示详细版文章列表（包括作者、分类、标签、摘要、分页等）| simple => 显示简约版文章列表（仅标题和日期）| none 不显示文章列表
# postList: detailed
postList: none
# simplePostListLength: 10 # 简约版文章列表显示的文章数量，默认10。（仅在postList设置为simple时生效）
---
<Home />
::: center
  ## 快速上手
:::

<code-group>
  <code-block title="Ubuntu 20.04" active>
  ```bash

  # 1. 安装依赖
  sudo apt-get update && sudo apt-get install binutils git git-lfs gnupg flex bison gperf \
  build-essential zip curl zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 lib32ncurses5-dev \
  x11proto-core-dev libx11-dev lib32z1-dev ccache libgl1-mesa-dev libxml2-utils xsltproc unzip m4 bc \
  gnutls-bin python3.8 python3-pip

  # 2. 安装工具：repo 和 hc-gen
  sudo curl -s https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 > /usr/local/bin/repo  #默认使用root权限安装至/usr/local/bin,也可以装至其他路径
  sudo chmod a+x /usr/local/bin/repo
  sudo pip3 install -i https://repo.huaweicloud.com/repository/pypi/simple requests
  wget https://repo.huaweicloud.com/harmonyos/compiler/hc-gen/0.65/linux/hc-gen-0.65-linux.tar 
  tar -xvf hc-gen-0.65-linux.tar 
  sudo cp hc-gen/hc-gen /usr/local/bin/hc-gen

  # 3. 配置git相关参数
  git config --global user.email "xxx@mail.com"
  git config --global user.name "xxx"

  # 4. 创建代码目录并拉取代码
  mkdir OpenHarmony
  cd OpenHarmony
  repo init -u https://gitee.com/openharmony/manifest.git -b OpenHarmony-2.2-Beta2 --no-repo-verify
  repo sync -c
  repo forall -c 'git lfs pull'

  # 5. 下载预编译工具
  ./build/prebuilts_download.sh
  
  # 6. 运行编译脚本（需自行调整参数）
  ./build.sh

  ```
  </code-block>

</code-group>

更多细节请参见[下载](/download/)页面。
