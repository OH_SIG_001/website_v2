---
title: 开发实例
permalink: /pages/01050a04
navbar: true
sidebar: true
prev: true
next: true
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:25
---
# 开发实例<a name="ZH-CN_TOPIC_0000001061399563"></a>

开发实例可参考[开源项目中的示例](https://gitee.com/openharmony/aafwk_aafwk_lite/tree/master/frameworks/ability_lite/example)

