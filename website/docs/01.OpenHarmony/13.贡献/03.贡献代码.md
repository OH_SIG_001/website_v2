---
title: 贡献代码
permalink: /pages/010d03
navbar: true
sidebar: true
prev: true
next: true
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:36
---
# 贡献代码<a name="ZH-CN_TOPIC_0000001051566732"></a>

## 开始贡献<a name="section123657169441"></a>

### 设计规范

[OpenHarmony架构设计原则](https://gitee.com/openharmony/community/blob/master/sig/sig-QA/%E6%9E%B6%E6%9E%84%E8%AE%BE%E8%AE%A1%E5%8E%9F%E5%88%99.md)

[OpenHarmony API治理章程](/pages/extra/261918/)

[OpenHarmony安全设计规范](/pages/extra/aec558/)

[OpenHarmony编译规范](https://gitee.com/openharmony/community/blob/master/sig/sig-QA/%E7%BC%96%E8%AF%91%E8%A7%84%E8%8C%83.md)

### 代码风格

请遵循OpenHarmony编程规范，进行代码开发、检视、测试，务必保持代码风格统一。

-   [C++语言编程规范](/pages/extra/ca40b1/)
-   [C语言编程规范](/pages/extra/ac1933/)
-   [JavaScript语言编程规范](/pages/extra/ae0e51/)
-   [Python语言编程规范](https://pep8.org/)
-   [C&C++语言安全编程指南](/pages/extra/1f6576/)
-   [Java语言安全编程指南](/pages/extra/d49c41/)
-   [32/64位可移植编程规范](/pages/extra/a53c98/)
-   [HDF驱动编程规范](/pages/extra/a7ae41/)
-   [Log打印规范](/pages/extra/2d2a23/)

### 开源软件引入

若要引入新的第三方开源软件到OpenHarmony项目中，请参考[第三方开源软件引入指导](/pages/extra/0491e3/)

## 贡献工作流<a name="section15769105812369"></a>

有关详细信息，请参考[贡献流程](/pages/010d04)。

[代码门禁详细质量要求](https://gitee.com/openharmony/community/blob/master/sig/sig-QA/%E4%BB%A3%E7%A0%81%E9%97%A8%E7%A6%81%E8%A6%81%E6%B1%82.md)。

## 社区安全问题披露<a name="section725624119448"></a>

-   [安全问题处理和发布流程](https://gitee.com/openharmony/security/blob/master/zh/security-process/README.md)

-   [安全和披露说明](https://gitee.com/openharmony/security/blob/master/zh/security-process/security-disclosure.md)


