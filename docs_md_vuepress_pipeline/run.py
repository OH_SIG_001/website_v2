# Copyright (c) 2021 changwei@iscas.ac.cn
# 
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this list of
#    conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice, this list
#    of conditions and the following disclaimer in the documentation and/or other materials
#    provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its contributors may be used
#    to endorse or promote products derived from this software without specific prior written
#    permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
import datetime
import os
import sys

sys.path.insert(0, os.path.abspath('.'))
os.system('mkdir -p out/')
os.system('mkdir -p docs_vuepress/docs')

INPUT_DOC_ROOT = os.path.join(os.getcwd(), 'docs/zh-cn')
OUTPUT_DOC_ROOT = os.path.join(os.getcwd(), 'docs_vuepress/docs')

TIME_STAMP = datetime.datetime.now().timestamp()

DOC_TREE_FILENAME = os.path.join(INPUT_DOC_ROOT, 'website-directory.md')
RAW_DOC_JSON_FILENAME = f'out/{TIME_STAMP}s1-raw.json'
NOT_FOUND_DOC_TREE_FILENAME = f'out/{TIME_STAMP}s1-not_found.txt'
ENHANCED_DOC_INDEX_FILENAME = f'out/{TIME_STAMP}s2-doc_index.json'
DOC_TABLE_FILENAME = f'out/{TIME_STAMP}s3-doc_table.json'

POSTS_DOC_TABLE_FILENAME = f'out/{TIME_STAMP}s4-posts_table.json'
MODIFIED_LIST_FILENAME = f'out/{TIME_STAMP}s4-mod.json'
NOT_FOUND_LIST_FILENAME = f'out/{TIME_STAMP}s4-not_mod.json'


def main():
    parse_doc_tree_main(DOC_TREE_FILENAME, RAW_DOC_JSON_FILENAME)
    check_file_list_existence(RAW_DOC_JSON_FILENAME, INPUT_DOC_ROOT,
                              NOT_FOUND_DOC_TREE_FILENAME)
    enhance_json_info_main(RAW_DOC_JSON_FILENAME, ENHANCED_DOC_INDEX_FILENAME,
                           INPUT_DOC_ROOT)
    copy_md_main(ENHANCED_DOC_INDEX_FILENAME, INPUT_DOC_ROOT, OUTPUT_DOC_ROOT,
                 MODIFIED_LIST_FILENAME, NOT_FOUND_LIST_FILENAME)


if __name__ == '__main__':
    from step_1_parse_doc_tree import parse_doc_tree_main, check_file_list_existence
    from step_2_enhance_json_info import enhance_json_info_main
    from step_3_copy_md import copy_md_main

    main()
