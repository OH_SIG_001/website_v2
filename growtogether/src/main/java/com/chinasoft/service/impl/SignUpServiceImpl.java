package com.chinasoft.service.impl;

import com.chinasoft.bean.po.UserSubject;
import com.chinasoft.bean.vo.SignUpVO;
import com.chinasoft.mapper.SignUpMapper;
import com.chinasoft.service.SignUpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;

@Service
public class SignUpServiceImpl implements SignUpService {

    @Autowired
    private SignUpMapper signUpMapper;

    @Override
    public void saveSignUp(SignUpVO signUpVO) {
        signUpMapper.saveSignUp(signUpVO);
        List<Map<String,Object>> list=signUpMapper.getSignUpId();
        if(list.size()>0){
           int signUpId=Integer.parseInt(list.get(0).get("id").toString());
            UserSubject userSubject=new UserSubject();
            userSubject.setSignUpId(signUpId);
            userSubject.setSubjectId(signUpVO.getSubjectId());
            signUpMapper.saveUserSubject(userSubject);
        }
    }

    @Override
    public List<Map<String, Object>> getSubjectByCategory(String project) {
        return signUpMapper.getSubjectByCategory(project);
    }

    @Override
    public List<Map<String, Object>> getStatusBySubjectId(String subjectId) {
        return signUpMapper.getStatusBySubjectId(subjectId);
    }

    @Override
    public List<Map<String, Object>> getSignBySubject(String subjectId) {
        return signUpMapper.getSignBySubject(subjectId);
    }

    @Override
    public void updateSignUpStatus(String subjectId,String signUpStatus,String userId) {
        signUpMapper.updateSignUpStatus(subjectId,signUpStatus,userId);
    }

    @Override
    public List<Map<String, Object>> getSingnByPhone(String phone) {
        return signUpMapper.getSingnByPhone(phone);
    }

    @Override
    public List<Map<String, Object>> getSingStatusBySingId(String singId) {
        return signUpMapper.getSingStatusBySingId(singId);
    }

    @Override
    public List<Map<String, Object>> getUserSubject(String userId) {
        return signUpMapper.getUserSubject(userId);
    }

    @Override
    public List<Map<String, Object>> getSubject(String subjectId,String userId) {
        return signUpMapper.getSubject(subjectId,userId);
    }

    @Override
    public void saveKnotImage(String userId, String knotImage) {
        signUpMapper.saveKnotImage(userId,knotImage);
    }

    @Override
    public List<Map<String, Object>> getSignTime(String userId) {
        return signUpMapper.getSignTime(userId);
    }
}
