import Vue from "vue";
import App from "./App.vue";
import axios from "axios";

Vue.config.productionTip = false;
import "./assets/less/common.less";
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import "./assets/style/font/iconfont.css";
Vue.use(ElementUI);
axios.defaults.baseURL = "http://localhost:8188/sign"; //云
// axios.defaults.baseURL = "http://112.95.225.82:9088/autotool"; //后台
// axios.defaults.baseURL = "http:/127.0.0.1:9088/autotool/"; //远程
Vue.prototype.$axios = axios;

import utils from "@/utils/utils.js";
Vue.prototype.$utils = utils;

import router from "./router";

axios.interceptors.request.use(
  function(config) {
    return config;
  },
  function(error) {
    return Promise.reject(error);
  }
);
// 响应请求拦截器

axios.interceptors.response.use(
  function(response) {
    if (response.data.code != 0) {
      Vue.prototype.$message.warning(response.data.msg);
    }
    return response;
  },
  function(error) {
    return Promise.reject(error);
  }
);
new Vue({
  render: (h) => h(App),
  router,
}).$mount("#app");
