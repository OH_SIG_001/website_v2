import Vue from "vue";
import VueRouter from "vue-router";

const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err);
};
Vue.use(VueRouter);

let routes = [
  {
    path: "/",
    component: () => import("./components/index.vue"),
    children: [
      {
        path: "mainPlay",
        component: () => import("./components/mainPlay.vue"),
        meta: {
          nologin: "noLogin",
        },
      },
      {
        path: "/",
        redirect: "mainPlay",
      },
      {
        path: "login",
        component: () => import("./components/login.vue"),
      },
      {
        path: "user",
        component: () => import("./components/user.vue"),
        meta: {
          nologin: "logining",
        },
      },
      {
        path: "userloading",
        component: () => import("./components/userloading.vue"),
        meta: {
          nologin: "logining",
        },
      },
      {
        path: "userNext",
        component: () => import("./components/userNext.vue"),
        meta: {
          nologin: "logining",
        },
      },
      {
        path: "detail",
        component: () => import("./components/detail.vue"),
        meta: {
          nologin: "noLogin",
        },
      },
      {
        path: "register",
        component: () => import("./components/register.vue"),
        meta: {
          nologin: "noLogin",
        },
      },
      {
        path: "registerSuccess",
        component: () => import("./components/registerSuccess.vue"),
        meta: {
          nologin: "noLogin",
        },
      },
    ],
  },
];

let router = new VueRouter({
  mode: "history",
  routes,
});
export default router;
