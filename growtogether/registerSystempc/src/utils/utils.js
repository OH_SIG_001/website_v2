export default {
  //校验中英文姓名
  checkUserName(rule, value, callback) {
    if (!value) {
      return callback(new Error("姓名不能为空"));
    } else {
      const reg = /^[A-Za-z\u4e00-\u9fa5]+$/;
      console.log(value, reg.test(value));
      if (reg.test(value)) {
        callback();
      } else {
        return callback(new Error("格式错误！请输入中英文字符"));
      }
    }
  },
  //校验地址
  checkUserAdress(rule, value, callback) {
    if (!value) {
      return callback(new Error("地址不能为空"));
    } else {
      callback();
    }
  },
  //Gitee账号
  checkUserGitee(rule, value, callback) {
    if (!value) {
      return callback(new Error("Gitee账号不能为空"));
    } else {
      callback();
    }
  },
  //校验手机号
  checkPhone(rule, value, callback) {
    if (!value) {
      return callback(new Error("手机号码不能为空"));
    } else {
      const reg = /^1\d{10}$/;
      if (reg.test(value)) {
        callback();
      } else {
        callback(new Error("请输入11位手机号码"));
      }
    }
  },
  //校验邮箱
  checkEmail(rule, value, callback) {
    if (!value) {
      return callback(new Error("email不能为空"));
    } else {
      const regEmail = /^([a-zA-Z]|[0-9])(\w|\-)+@[a-zA-Z0-9]+\.([a-zA-Z]{2,3})$/;
      if (regEmail.test(value)) {
        callback();
      } else {
        callback(new Error("电子邮箱格式错误！请输入正确邮箱"));
      }
    }
  },
};
