import Vue from "vue";
import VueRouter from "vue-router";

// const originalPush = VueRouter.prototype.push;
// VueRouter.prototype.push = function push(location) {
//   return originalPush.call(this, location).catch((err) => err);
// };
Vue.use(VueRouter);

let routes = [
  {
    path: "/",
    component: () => import("./components/index.vue"),
    children: [
      {
        path: "mainPlay", //首页
        name: "mainPlay",
        component: () => import("./components/mainPlay.vue"),
        meta: { title: "首页", name: "mainPlay" }
      },
      {
        path: "activityList", //活动详情
        name: "activityList",
        component: () => import("./components/activityList.vue"),
        meta: { title: "活动", name: "activityList" }
      },
      {
        path: "newDetail", //三级详情
        name: "newDetail",
        component: () => import("./components/newDetail.vue"),
      },
      {
        path: "blogList", //博客详情
        name: "blogList",
        component: () => import("./components/blogList.vue"),
        meta: { title: "博客", name: "blogList" }
      },
      {
        path: "newList", //新闻详情
        name: "newList",
        component: () => import("./components/newList.vue"),
        meta: { title: "新闻", name: "newList" }
      },
      {
        path: "armList", //开发板详情
        name: "armList",
        component: () => import("./components/armList.vue"),
      },
      {
        path: "liveList", //直播详情
        name: "liveList",
        component: () => import("./components/liveList.vue"),
      },
      {
        path: "videoList", //视频详情
        name: "videoList",
        component: () => import("./components/videoList.vue"),
      },
      // {
      //   path: "allFeatures", 
      //   name: "allFeatures",
      //   component: () => import("./components/allFeatures.vue"),
      // },
      {
        path: "menuBar", //手机端菜单页
        component: () => import("./components/menuBar.vue"),
      },
      {
        path: "/",
        redirect: "mainPlay",
      },
    ],
  },
];

let router = new VueRouter({
  mode: "history",
  routes,
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
});
export default router;
