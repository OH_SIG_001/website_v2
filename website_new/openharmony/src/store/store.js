import Vue from 'vue'
import Vuex from 'vuex'

//挂载Vuex
Vue.use(Vuex)

//创建VueX对象
import state from "./state"
import getters from "./getters.js"
import mutations from "./mutations.js"
import actions from "./actions.js"
import meeting from './modules/meeting.js'
import activity from './modules/activity.js'
import devBoard from './modules/devBoard.js'
import banner from './modules/banner.js'
import subBanner from './modules/subBanner.js'
import subData from './modules/subData.js'
import developData from './modules/developData.js'
import threePage from './modules/threePage.js'
import likeData from './modules/likeData.js'
const store = new Vuex.Store({
    state,
    getters,
    mutations,
    actions,
    modules: {
        meeting,
        activity,
        devBoard,
        banner,
        subBanner,
        subData,
        developData,
        threePage,
        likeData
    }
})
export default store