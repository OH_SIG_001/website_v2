import { queryBanner } from "../../api/index"
export default {
    state: {
        bannerList: [],
    },
    getters: {

    },
    mutations: {
        getQueryBanner(state, data) {
            state.bannerList = data;
            console.log(state.bannerList);
        },

    },
    actions: {
        async getQueryBanner({ commit }, category) {
            const { data } = await queryBanner(category);
            if (data.code === 0) {
                console.log("你好", data);
                commit("getQueryBanner", data.data);
            }
        },

    }
}