import Vue from "vue";
import VueRouter from "vue-router";

const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err);
};
Vue.use(VueRouter);

let routes = [
  {
    path: "/",
    component: () => import("./components/index.vue"),

    children: [
      {
        path: "/mainPlay",
        name: "mainPlay",
        component: () => import("./components/mainPlay.vue"),
        children: [
          {
            path: "/mainPlay/content",
            name: "content",
            component: () => import("./components/content.vue"),
            children: [
              {
                path: "/mainPlay/content/mainText",
                name: "mainText",
                component: () => import("./components/mainText.vue"),
                children: [
                  {
                    path: "/mainPlay/content/mainText/allFeatures",
                    name: "allFeatures",
                    component: () => import("./components/allFeatures.vue"),
                  },
                  {
                    path: "/",
                    redirect: "allFeatures",
                  },
                ]
              },
              {
                path: "/",
                redirect: "mainText",
              },
            ],
          },
          {
            path: "/mainPlay/detail",
            name: "detail",
            component: () => import("./components/detail.vue"),
          },
          {
            path: "/",
            redirect: "content",
          },
        ],
      },
      // {
      //   path: "/detail",
      //   name: "detail",
      //   component: () => import("./components/detail.vue"),
      // },
      {
        path: "/scrollNav",
        name: "scrollNav",
        component: () => import("./components/scrollNav.vue"),
      },

      {
        path: "/",
        redirect: "mainPlay",
      },
    ],
  },
];

let router = new VueRouter({
  mode: "history",
  routes,
});
export default router;
