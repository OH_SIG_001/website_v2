/*
 *Copyright(c) 2021 Shenzhen Kaihong Digital Industry DevelopmentCo.,Ltd
 *Licensed under the Apache License,Version2.0(the"License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.controller;

import com.chinasoft.annotation.OperationLog;
import com.chinasoft.bean.po.Level;
import com.chinasoft.service.LevelService;
import com.chinasoft.utils.PageListVO;
import com.chinasoft.utils.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 级别模块
 *
 * @author dengtao
 */
@CrossOrigin
@RestController
@RequestMapping("/level")
@Api(tags = "级别",description = "级别管理")
public class LevelController {

    private static final Logger logger = LoggerFactory.getLogger(LevelController.class);

    @Autowired
    private LevelService levelService;

    @ApiOperation(value = "级别信息保存")
    @PostMapping("/addLevel")
    @OperationLog(action = "级别信息[保存]", operateType = "level/save")
    public ResultVO save(@RequestBody Level level) {
        ResultVO resultVo = new ResultVO();
        try {
            if (null != level){
                Level levelVO = levelService.find(level);
                if (null != levelVO){
                    resultVo.setCode(ResultVO.FAIL);
                    resultVo.setMsg("级别名称已存在");
                    return resultVo;
                }
            }
            int res = levelService.add(level);
            logger.info("success > 0,failed <= 0, save result=" + res);
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("新增成功");
            resultVo.setData(res);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }


    @ApiOperation(value = "级别信息修改")
    @PostMapping("/editLevel")
    @OperationLog(action = "级别信息[修改]", operateType = "level/edit")
    public ResultVO edit(@RequestBody Level level) {
        ResultVO resultVo = new ResultVO();
        try {
            if (null == level){
                resultVo.setCode(ResultVO.FAIL);
                resultVo.setMsg("参数为空");
                return resultVo;
            }
            int res = levelService.edit(level);
            logger.info("success > 0,failed <= 0, save result=" + res);
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("修改成功");
            resultVo.setData(res);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }

    @ApiOperation(value = "级别信息删除")
    @PostMapping("/delLevel")
    @OperationLog(action = "级别信息[删除]", operateType = "level/remove")
    public ResultVO remove(@RequestBody Level level) {
        ResultVO resultVo = new ResultVO();
        try {
            if (null == level || level.getLevelId() == 0){
                resultVo.setCode(ResultVO.FAIL);
                resultVo.setMsg("参数为空");
                return resultVo;
            }
            int res = levelService.remove(level.getLevelId());
            logger.info("success > 0,failed <= 0, save result=" + res);
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("删除成功");
            resultVo.setData(res);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }

    @ApiOperation(value = "级别信息批量删除")
    @GetMapping("/removeBatch")
    @OperationLog(action = "级别信息[批量删除]", operateType = "level/removeBatch")
    public ResultVO removeBatch(@RequestParam(value = "ids",required = false) String ids) {
        ResultVO resultVo = new ResultVO();
        try {
            if (null == ids || "".equals(ids)){
                resultVo.setCode(ResultVO.FAIL);
                resultVo.setMsg("ids or proIds is null");
                return resultVo;
            }
            int res = levelService.removeBatch(ids);
            logger.info("success > 0,failed <= 0, save result=" + res);
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("remove batch level success");
            resultVo.setData(res);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }

    @ApiOperation(value = "级别信息单个查看")
    @GetMapping("/find")
    @OperationLog(action = "级别信息[单个查看]", operateType = "level/find")
    public ResultVO find(Level level) {
        ResultVO resultVo = new ResultVO();
        try {
            if (null ==  level){
                resultVo.setCode(ResultVO.FAIL);
                resultVo.setMsg("param is null");
                return resultVo;
            }
            Level levelVO = levelService.find(level);
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("find level success");
            resultVo.setData(levelVO);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }

    /**
     * 分页查询
     * @param level
     * @return pageListVO
     */
    @ApiOperation(value = "级别信息分页查询")
    @GetMapping("/queryLevel")
    @OperationLog(action = "级别信息[分页查询]", operateType = "level/findBatch")
    public PageListVO<List<Level>> findBatch(Level level){
        PageListVO<List<Level>> pageListVO = new PageListVO<>();
        if (null == level){
            level = new Level();
        }
        if (null == level.getPageNum() || level.getPageNum() <= 0){
            level.setPageNum(1);
        }
        if (null == level.getPageSize()){
            level.setPageSize(10);
        }
        List<Level> resultList = levelService.findBatch(level);
        Integer resultTotal = levelService.findTotal(level);
        int page = resultTotal/level.getPageSize() + (resultTotal%level.getPageSize() > 0 ? 1 : 0);
        pageListVO.setTotalPage(page);
        pageListVO.setData(resultList);
        pageListVO.setCode(PageListVO.SUCCESS);
        pageListVO.setTotalNum(resultTotal);
        pageListVO.setPageNum(level.getPageNum());
        pageListVO.setPageSize(level.getPageSize());
        return pageListVO;
    }

}
