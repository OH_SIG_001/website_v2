/*
 *Copyright(c) 2021 Shenzhen Kaihong Digital Industry DevelopmentCo.,Ltd
 *Licensed under the Apache License,Version2.0(the"License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.controller;

import com.chinasoft.annotation.OperationLog;
import com.chinasoft.bean.po.HomeContent;
import com.chinasoft.bean.po.HomeContentPic;
import com.chinasoft.bean.po.HomeDevelopment;
import com.chinasoft.service.SecondaryPageService;
import com.chinasoft.utils.PageListVO;
import com.chinasoft.utils.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 1230官网二级页
 *
 * @author dengtao
 */

@CrossOrigin
@RestController
@RequestMapping("/secondaryPage")
@Api(tags = "二级页", description = "1230官网首页")
public class SecondaryPageController {

    private static final Logger logger = LoggerFactory.getLogger(SecondaryPageController.class);

    @Autowired
    private SecondaryPageService secondaryPageService;

    @ApiOperation(value = "二级页轮播图")
    @GetMapping("/queryCarousel")
    @OperationLog(action = "查询", operateType = "query")
    public ResultVO queryCarousel(@RequestParam("type") Integer type, @RequestParam("category") String category) {
        ResultVO resultVo = new ResultVO();
        try {
            if (null == type || type <= 0) {
                resultVo.setCode(ResultVO.FAIL);
                logger.error("type is null!");
                resultVo.setMsg("参数不能为空");
                return resultVo;
            }

            List<HomeContentPic> resultList = secondaryPageService.queryCarousel(type, category);
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("查询成功");
            resultVo.setData(resultList);
        } catch (Exception e) {
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
            e.printStackTrace();
        }
        return resultVo;
    }

    @ApiOperation(value = "二级页分页查询")
    @GetMapping("/queryBatch")
    @OperationLog(action = "查询", operateType = "query")
    public PageListVO queryBatch(HomeContent content) {
        PageListVO<List<HomeContent>> pageListVO = new PageListVO<>();
        if (null == content) {
            content = new HomeContent();
        }
        if (null == content.getPageNum() || content.getPageNum() <= 0) {
            content.setPageNum(1);
        }
        if (null == content.getPageSize()) {
            content.setPageSize(10);
        }
        List<HomeContent> resultList = secondaryPageService.queryBatch(content);
        Integer resultTotal = secondaryPageService.queryTotal(content);
        int page = resultTotal / content.getPageSize() + (resultTotal % content.getPageSize() > 0 ? 1 : 0);
        pageListVO.setTotalPage(page);
        pageListVO.setData(resultList);
        pageListVO.setCode(PageListVO.SUCCESS);
        pageListVO.setTotalNum(resultTotal);
        pageListVO.setPageNum(content.getPageNum());
        pageListVO.setPageSize(content.getPageSize());
        return pageListVO;
    }

    @ApiOperation(value = "三级页详情查询")
    @GetMapping("/queryDetails")
    @OperationLog(action = "查询", operateType = "query")
    public ResultVO queryDetails(@RequestParam("id") Integer id) {
        ResultVO resultVo = new ResultVO();
        try {
            if (null == id || id <= 0) {
                resultVo.setCode(ResultVO.FAIL);
                logger.error("id is null!");
                resultVo.setMsg("参数不能为空");
                return resultVo;
            }
            HomeContent homeContent = secondaryPageService.queryDetails(id);
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("查询成功");
            resultVo.setData(homeContent);
        } catch (Exception e) {
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }

    @ApiOperation(value = "修改点赞数、浏览量、分享次数")
    @PostMapping("/updateCount")
    @OperationLog(action = "修改", operateType = "edit")
    public ResultVO updateCount(@RequestParam("id") Integer id, @RequestParam("type") Integer type) {
        ResultVO resultVo = new ResultVO();
        try {
            if (null == id || id <= 0 || null == type || type <= 0) {
                resultVo.setCode(ResultVO.FAIL);
                logger.error("id or type is null!");
                resultVo.setMsg("参数不能为空");
                return resultVo;
            }
            int res = secondaryPageService.updateCount(id, type);

            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("更新成功");
            resultVo.setData(res);
        } catch (Exception e) {
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }

    @ApiOperation(value = "开发板二级页分页查询")
    @GetMapping("/queryDevelopmentBatch")
    @OperationLog(action = "查询", operateType = "query")
    public PageListVO queryDevelopmentBatch(HomeDevelopment dept) {
        PageListVO<List<HomeDevelopment>> pageListVO = new PageListVO<>();
        if (null == dept) {
            dept = new HomeDevelopment();
        }
        if (null == dept.getPageNum() || dept.getPageNum() <= 0) {
            dept.setPageNum(1);
        }
        if (null == dept.getPageSize()) {
            dept.setPageSize(10);
        }
        List<HomeDevelopment> resultList = secondaryPageService.queryDevelopmentBatch(dept);
        Integer resultTotal = secondaryPageService.queryDevelopmentTotal(dept);
        int page = resultTotal / dept.getPageSize() + (resultTotal % dept.getPageSize() > 0 ? 1 : 0);
        pageListVO.setTotalPage(page);
        pageListVO.setData(resultList);
        pageListVO.setCode(PageListVO.SUCCESS);
        pageListVO.setTotalNum(resultTotal);
        pageListVO.setPageNum(dept.getPageNum());
        pageListVO.setPageSize(dept.getPageSize());
        return pageListVO;
    }
}
