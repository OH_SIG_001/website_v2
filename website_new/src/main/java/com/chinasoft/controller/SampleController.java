/*
 *Copyright(c) 2021 Shenzhen Kaihong Digital Industry DevelopmentCo.,Ltd
 *Licensed under the Apache License,Version2.0(the"License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.controller;

import com.chinasoft.annotation.OperationLog;
import com.chinasoft.bean.vo.AddSampleVo;
import com.chinasoft.service.SampleService;
import com.chinasoft.utils.PageListVO;
import com.chinasoft.utils.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;

/**
 * 开发样例菜单
 * @author dengtao
 */

@CrossOrigin
@RestController
@RequestMapping("/sample")
@Api(tags = "开发样例",description = "开发样例菜单")
public class SampleController {

    private static final Logger logger = LoggerFactory.getLogger(SampleController.class);

    @Autowired
    private SampleService sampleService;

    @ApiOperation(value = "开发样例分页查询")
    @GetMapping("/querySample")
    @OperationLog(action = "查询", operateType = "query")
    public PageListVO querySample(@RequestParam("pageSize") int pageSize,
                                  @RequestParam("pageNum") int pageNum,
                                  @RequestParam("featuresId") String featuresId) {
        PageListVO resultVo = new PageListVO();
        try {
            if (pageSize != 10 && pageSize != 20 && pageSize != 50) {
                resultVo.setCode(1);
                resultVo.setMsg("页码大小错误");
            } else if (pageNum < 1) {
                resultVo.setCode(1);
                resultVo.setMsg("请求页码错误");
            } else {
                HashMap<String, String> params = new HashMap<>();
                params.put("pageSize", pageSize + "");
                int start = (pageNum - 1) * pageSize;
                System.err.println(start);
                params.put("start", start + "");
                params.put("featuresId",featuresId);
                resultVo.setPageNum(pageNum);
                resultVo.setPageSize(pageSize);
                resultVo.initPages(sampleService.queryPageNum(params));
                resultVo.setData(sampleService.querySample(params));
                resultVo.setCode(ResultVO.SUCCESS);
                resultVo.setMsg("查询成功");
            }
        } catch (Exception e) {
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }

    @ApiOperation(value = "开发样例特性与相关资料查询")
    @GetMapping("/queryInformation")
    @OperationLog(action = "查询", operateType = "query")
    public ResultVO queryInformation(@RequestParam("busTypeId") String busTypeId) {
        ResultVO resultVo = new ResultVO();
        try {
            resultVo.setData(sampleService.queryInformation(busTypeId));
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("查询成功");
        } catch (Exception e) {
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }
    @ApiOperation(value = "特性查询")
    @GetMapping("/queryData")
    @OperationLog(action = "查询", operateType = "query")
    public ResultVO queryData() {
        ResultVO resultVo = new ResultVO();
        try {
            resultVo.setData(sampleService.queryData());
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("查询成功");
        } catch (Exception e) {
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }

    @ApiOperation(value = "开发样例新增")
    @PostMapping("/addSample")
    @OperationLog(action = "新增", operateType = "add")
    public ResultVO addSample(@RequestBody AddSampleVo addSampleVo) {
        ResultVO resultVo = new ResultVO();
        try {
            sampleService.addSample(addSampleVo);
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("新增成功");
        } catch (Exception e) {
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }

    @ApiOperation(value = "开发样例编辑")
    @PostMapping("/editSample")
    @OperationLog(action = "编辑", operateType = "edit")
    public ResultVO editSample(@RequestBody AddSampleVo addSampleVo) {
        ResultVO resultVo = new ResultVO();
        try {
            sampleService.updateSample(addSampleVo);
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("编辑成功");
        } catch (Exception e) {
            e.printStackTrace();
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }

    @ApiOperation(value = "开发样例删除")
    @PostMapping("/deleteSample")
    @OperationLog(action = "删除", operateType = "delete")
    public ResultVO deleteSample(@RequestBody AddSampleVo addSampleVo) {
        ResultVO resultVo = new ResultVO();
        try {
            sampleService.deleteSample(addSampleVo);
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("删除成功");
        } catch (Exception e) {
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }

}
