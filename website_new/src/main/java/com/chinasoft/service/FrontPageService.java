/*
 *Copyright(c) 2021 Shenzhen Kaihong Digital Industry DevelopmentCo.,Ltd
 *Licensed under the Apache License,Version2.0(the"License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.service;

import com.alibaba.fastjson.JSONObject;
import java.util.HashMap;

public interface FrontPageService {
    /**
     * 知识图谱首页初始化数据
     * @param params
     * @return
     */
    JSONObject getFrontPageData(HashMap<String, String> params);

    /**
     * 知识图谱开发样例详情数据
     * @param sampleId
     * @return
     */
    JSONObject getFrontPageDetailData(String sampleId);
}
