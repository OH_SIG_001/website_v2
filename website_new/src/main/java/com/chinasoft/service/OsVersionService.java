/*
 *Copyright(c) 2021 Shenzhen Kaihong Digital Industry DevelopmentCo.,Ltd
 *Licensed under the Apache License,Version2.0(the"License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.service;

import com.chinasoft.bean.po.OsVersion;

import java.util.List;

public interface OsVersionService {
    int add(OsVersion osVersion);
    int edit(OsVersion osVersion);
    int remove(Integer id);
    int removeBatch(String ids);
    OsVersion find(OsVersion osVersion);
    List<OsVersion> findAll(OsVersion osVersion);
    List<OsVersion> findBatch(OsVersion osVersion);
    int findTotal(OsVersion osVersion);
}
