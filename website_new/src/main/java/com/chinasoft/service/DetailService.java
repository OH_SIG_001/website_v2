/*
 *Copyright(c) 2021 Shenzhen Kaihong Digital Industry DevelopmentCo.,Ltd
 *Licensed under the Apache License,Version2.0(the"License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.service;

import com.chinasoft.bean.po.Detail;

import java.util.List;

public interface DetailService {
    int add(Detail detail);
    int edit(Detail detail);
    int remove(Integer id);
    int removeBatch(String ids);
    Detail find(Detail detail);
    List<Detail> findAll(Detail detail);
    List<Detail> findBatch(Detail detail);
    int findTotal(Detail detail);
}
