/*
 *Copyright(c) 2021 Shenzhen Kaihong Digital Industry DevelopmentCo.,Ltd
 *Licensed under the Apache License,Version2.0(the"License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.chinasoft.mapper.FrontPageMapper;
import com.chinasoft.service.FrontPageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class FrontPageServiceImpl implements FrontPageService {

    @Autowired
    private FrontPageMapper frontPageMapper;

    @Override
    public JSONObject getFrontPageData(HashMap<String, String> params) {
        List<Map<String, Object>> titleList = new ArrayList<>();
        //List<Map<String,Object>> levelList=new ArrayList<>();
        List<Map<String, Object>> osTypeList = new ArrayList<>();
        List<Map<String, Object>> osVersionList = new ArrayList<>();
        List<Map<String, Object>> busTypeList = new ArrayList<>();
        List<Map<String, Object>> featuresList = new ArrayList<>();
        List<Map<String, Object>> sampleList = new ArrayList<>();
        JSONObject object = new JSONObject();
        //标题集合
        titleList = frontPageMapper.getTitleData();
        if (titleList.size() > 0) {
            object.put("titleList", titleList);
        } else {
            object.put("titleList", null);
        }
        //os类型集合
        osTypeList = frontPageMapper.getOsTypeData();
        if (osTypeList.size() > 0) {
            object.put("osTypeList", osTypeList);
        } else {
            object.put("osTypeList", null);
        }
        //os版本集合
        osVersionList = frontPageMapper.getOsVersionData();
        if (osVersionList.size() > 0) {
            object.put("osVersionList", osVersionList);
        } else {
            object.put("osVersionList", null);
        }
        /*//级别集合
        levelList=frontPageMapper.getLevelData();
        if(levelList.size()>0){
            object.put("levelList",levelList);
        }else{
            object.put("levelList",null);
        }*/

        //业务类型集合
        if (titleList.size() > 0 && osTypeList.size() > 0 && osVersionList.size() > 0) {
            //第一次进入首页时,默认查询第一个标题与第一个级别
            if (params.get("titleId") != null) {
                busTypeList = frontPageMapper.getBusTypeData(params.get("titleId"));
            } else {
                busTypeList = frontPageMapper.getBusTypeData(titleList.get(0).get("titleId").toString());
            }
            object.put("busTypeList", busTypeList);
        } else {
            object.put("busTypeList", null);
        }
        //特性集合
        if (busTypeList.size() > 0) {
            if (params.get("busTypeId") != null) {
                featuresList = frontPageMapper.getFeaturesData(params.get("busTypeId"));
            } else {
                featuresList = frontPageMapper.getFeaturesData(busTypeList.get(0).get("busTypeId").toString());
            }
            object.put("featuresList", featuresList);
        } else {
            object.put("featuresList", null);
        }
        if (params.get("busTypeId") == null) {
            frontPageMapper.getBusTypeData(titleList.get(0).get("titleId").toString());
            params.put("busTypeId", busTypeList.get(0).get("busTypeId").toString());
        }
        //开发样例集合
        if (featuresList.size() > 0) {
            /*//为null时代表全部特性 ,不为null时查询单个特性id下面的开发样例
            if(params.get("featuresId")!=null){
                sampleList.addAll(frontPageMapper.getSampleData(params.get("featuresId")));
            }else{
                //查询开发样例数据
                for(int i=0;i<featuresList.size();i++){
                    sampleList.addAll(frontPageMapper.getSampleData(featuresList.get(i).get("featuresId").toString()));
                }
            }*/

            if (params.get("titleId") != null && params.get("osTypeId") != null && params.get("osVersionId") != null && params.get("busTypeId") != null && params.get("featuresId") != null) {
                String osTypeIds[] = params.get("osTypeId").split(",");
                String osVersionIds[] = params.get("osVersionId").split(",");
                for (int i = 0; i < osTypeIds.length; i++) {
                    for (int k = 0; k < osVersionIds.length; k++) {
                        System.err.println("osTypeIds[i]===" + osTypeIds[i] + "----osVersionIds[k]====" + osVersionIds[k]);
                        sampleList.addAll(frontPageMapper.getSample1(params.get("titleId"), osTypeIds[i], osVersionIds[k], params.get("busTypeId"), params.get("featuresId")));
                    }
                }
            } else if (params.get("titleId") != null && params.get("osTypeId") != null && params.get("osVersionId") != null && params.get("busTypeId") != null) {
                String osTypeIds[] = params.get("osTypeId").split(",");
                String osVersionIds[] = params.get("osVersionId").split(",");
                for (int i = 0; i < osTypeIds.length; i++) {
                    for (int k = 0; k < osVersionIds.length; k++) {
                        System.err.println("osTypeIds[i]===" + osTypeIds[i] + "----osVersionIds[k]====" + osVersionIds[k]);
                        sampleList.addAll(frontPageMapper.getSample2(params.get("titleId"), osTypeIds[i], osVersionIds[k], params.get("busTypeId")));
                    }
                }
            } else if (params.get("titleId") != null && params.get("osTypeId") != null && params.get("osVersionId") != null) {
                String osTypeIds[] = params.get("osTypeId").split(",");
                String osVersionIds[] = params.get("osVersionId").split(",");
                for (int i = 0; i < osTypeIds.length; i++) {
                    for (int k = 0; k < osVersionIds.length; k++) {
                        System.err.println("osTypeIds[i]===" + osTypeIds[i] + "----osVersionIds[k]====" + osVersionIds[k]);
                        sampleList.addAll(frontPageMapper.getSample3(params.get("titleId"), osTypeIds[i], osVersionIds[k]));
                    }
                }
            } else if (params.get("titleId") != null && params.get("osTypeId") != null) {
                String osTypeIds[] = params.get("osTypeId").split(",");
                for (int i = 0; i < osTypeIds.length; i++) {
                    sampleList.addAll(frontPageMapper.getSample4(params.get("titleId"), osTypeIds[i]));
                }
            } else if (params.get("titleId") != null) {
                sampleList.addAll(frontPageMapper.getSample5(params.get("titleId"), params.get("busTypeId")));
            } else {
                sampleList.addAll(frontPageMapper.getSample5(titleList.get(0).get("titleId").toString(), params.get("busTypeId")));
            }

            //删除ArrayList中重复元素，保持顺序
            HashSet hashSet = new HashSet();
            List newList = new ArrayList();
            for (Iterator iter = sampleList.iterator(); iter.hasNext(); ) {
                Object element = iter.next();
                if (hashSet.add(element)) {
                    newList.add(element);
                }
            }
            sampleList.clear();
            sampleList.addAll(newList);
            //循环获取相关类型并放入集合
            for (int k = 0; k < sampleList.size(); k++) {
                String typeName = frontPageMapper.getTypeNameData(sampleList.get(k).get("sampleId").toString());
                sampleList.get(k).put("typeName", typeName);
            }
            Collections.sort(sampleList, new Comparator<Map<String, Object>>() {
                @Override
                public int compare(Map<String, Object> o1, Map<String, Object> o2) {
                    Integer name1 = Integer.valueOf(o1.get("sign").toString());//name1是从你list里面拿出来的一个
                    Integer name2 = Integer.valueOf(o2.get("sign").toString()); //name1是从你list里面拿出来的第二个name
                    return name2.compareTo(name1);
                }
            });
            //集合分页
            int size = sampleList.size();
            int count = Integer.parseInt(params.get("pageSize"));
            int page = Integer.parseInt(params.get("pageNum"));
            int pageCount = size / count;
            int fromIndex = count * (page - 1);
            int toIndex = fromIndex + count;
            if (toIndex >= size) {
                toIndex = size;
            }
            if (page > pageCount + 1) {
                fromIndex = 0;
                toIndex = 0;
            }
            object.put("sampleList", sampleList.subList(fromIndex, toIndex));
            //放入总数
            if (sampleList.size() > 0) {
                object.put("totalNum", sampleList.size());
                int pages = sampleList.size() / count + (sampleList.size() % count > 0 ? 1 : 0);
                object.put("totalPage", pages);
            } else {
                object.put("totalNum", 1);
                object.put("totalPage", 1);
            }
        } else {
            object.put("sampleList", null);
            object.put("totalNum", 0);
            object.put("totalPage", 0);
        }
        return object;
    }

    @Override
    public JSONObject getFrontPageDetailData(String sampleId) {
        List<Map<String, Object>> sampleDetailList = new ArrayList<>();
        List<Map<String, Object>> baseInfoList = new ArrayList<>();
        LinkedList typeDetailList = new LinkedList();
        JSONObject object = new JSONObject();
        //开发样例详情集合
        sampleDetailList = frontPageMapper.getSampleDetail(sampleId);
        if (sampleDetailList.size() > 0) {
            String featureName = "";
            //逗号拆分特性循环查询出特性名称
            String featureIds = sampleDetailList.get(0).get("featuresIds").toString();
            String str[] = featureIds.split(",");
            for (int i = 0; i < str.length; i++) {
                featureName += frontPageMapper.getFeatureNameData(str[i]) + ",";
            }
            //去除最后一个逗号
            featureName = featureName.substring(0, featureName.length() - 1);
            sampleDetailList.get(0).put("featureName", featureName);
            object.put("sampleDetailList", sampleDetailList);
        } else {
            object.put("sampleDetailList", null);
        }
        //基础信息集合
        baseInfoList = frontPageMapper.getBaseInfoData(sampleId);
        if (baseInfoList.size() > 0) {
            object.put("baseInfoList", baseInfoList);
        } else {
            object.put("baseInfoList", null);
        }
        //相关分类集合
        typeDetailList = frontPageMapper.getTypeDetailData(sampleId);
        for (int i = 0; i < typeDetailList.size(); i++) {
            System.err.println("typeDetailList==" + typeDetailList.get(i));
        }
        if (typeDetailList.size() > 0) {
            object.put("typeDetailList", typeDetailList);
        } else {
            object.put("typeDetailList", null);
        }
        return object;
    }
}

