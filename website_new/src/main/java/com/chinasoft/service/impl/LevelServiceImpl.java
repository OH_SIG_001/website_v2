/*
 *Copyright(c) 2021 Shenzhen Kaihong Digital Industry DevelopmentCo.,Ltd
 *Licensed under the Apache License,Version2.0(the"License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.service.impl;

import com.chinasoft.bean.po.Level;
import com.chinasoft.mapper.LevelMapper;
import com.chinasoft.service.LevelService;
import com.chinasoft.service.LevelService;
import com.google.common.collect.Lists;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LevelServiceImpl implements LevelService {

    private static Logger logger = Logger.getLogger(LevelServiceImpl.class);

    @Autowired
    private LevelMapper levelMapper;

    @Override
    public int add(Level level) {
        return levelMapper.insert(level);
    }

    @Override
    public int edit(Level level) {
        return levelMapper.update(level);
    }

    @Override
    public int remove(Integer id) {
        if (null == id){
            logger.error("id is null");
            return 0;
        }
        return levelMapper.delete(id);
    }

    @Override
    public int removeBatch(String ids) {
        if (StringUtils.isEmpty(ids) && StringUtils.isEmpty(ids)){
            logger.error("ids is null");
            return 0;
        }
        List<Integer> idsList = new ArrayList<>();
        if (!StringUtils.isEmpty(ids)){
           idsList = Lists.newArrayList(ids.split(",")).stream().map(id->Integer.parseInt(id)).collect(Collectors.toList());
        }
        return levelMapper.deleteBatch(idsList);
    }

    @Override
    public Level find(Level level) {
        return levelMapper.query(level);
    }

    @Override
    public List<Level> findAll(Level level) {
        return levelMapper.queryAll(level);
    }

    @Override
    public List<Level> findBatch(Level level) {
        return levelMapper.queryBatch(level);
    }

    @Override
    public int findTotal(Level level) {
        return levelMapper.queryTotal(level);
    }
}
