/*
 *Copyright(c) 2021 Shenzhen Kaihong Digital Industry DevelopmentCo.,Ltd
 *Licensed under the Apache License,Version2.0(the"License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.service.impl;

import com.chinasoft.bean.po.OsType;
import com.chinasoft.mapper.OsTypeMapper;
import com.chinasoft.service.OsTypeService;
import com.google.common.collect.Lists;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OsTypeServiceImpl implements OsTypeService {

    private static Logger logger = Logger.getLogger(OsTypeServiceImpl.class);

    @Autowired
    private OsTypeMapper osTypeMapper;

    @Override
    public int add(OsType osType) {
        return osTypeMapper.insert(osType);
    }

    @Override
    public int edit(OsType osType) {
        return osTypeMapper.update(osType);
    }

    @Override
    public int remove(Integer id) {
        if (null == id){
            logger.error("id is null");
            return 0;
        }
        return osTypeMapper.delete(id);
    }

    @Override
    public int removeBatch(String ids) {
        if (StringUtils.isEmpty(ids) && StringUtils.isEmpty(ids)){
            logger.error("ids or proIds is null");
            return 0;
        }
        List<Integer> idsList = new ArrayList<>();
        if (!StringUtils.isEmpty(ids)){
           idsList = Lists.newArrayList(ids.split(",")).stream().map(id->Integer.parseInt(id)).collect(Collectors.toList());
        }
        return osTypeMapper.deleteBatch(idsList);
    }

    @Override
    public OsType find(OsType osType) {
        return osTypeMapper.query(osType);
    }

    @Override
    public List<OsType> findAll(OsType osType) {
        return osTypeMapper.queryAll(osType);
    }

    @Override
    public List<OsType> findBatch(OsType osType) {
        return osTypeMapper.queryBatch(osType);
    }

    @Override
    public int findTotal(OsType osType) {
        return osTypeMapper.queryTotal(osType);
    }
}
