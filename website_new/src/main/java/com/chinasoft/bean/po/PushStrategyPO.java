/*
 *Copyright(c) 2021 Shenzhen Kaihong Digital Industry DevelopmentCo.,Ltd
 *Licensed under the Apache License,Version2.0(the"License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.bean.po;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value = "推送策略")
@Data
public class PushStrategyPO {

    //id
    @ApiModelProperty(value = "策略id")
    private Integer id;

    //邮件主题
    @ApiModelProperty(value = "邮件主题")
    private String submit;
    //邮件内容
    @ApiModelProperty(value = "邮件内容")
    private String content;
    //邮件主送人
    @ApiModelProperty(value = "邮件主送人")
    private String recipient;
    //邮件主送人
    @ApiModelProperty(value = "邮件抄送人")
    private String CC;

    @ApiModelProperty(value = "邮件附件路径")
    private String path;

    @ApiModelProperty(value = "邮件附件名称")
    private String fileName;

    //删除Del
    @ApiModelProperty(value = "逻辑删除，【0】为保存，【1】为删除")
    private int isDel;

}
