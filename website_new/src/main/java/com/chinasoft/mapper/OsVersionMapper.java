/*
 *Copyright(c) 2021 Shenzhen Kaihong Digital Industry DevelopmentCo.,Ltd
 *Licensed under the Apache License,Version2.0(the"License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.mapper;

import com.chinasoft.bean.po.OsVersion;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface OsVersionMapper {
    int insert(OsVersion osVersion);
    int update(OsVersion osVersion);
    int delete(Integer id);
    int deleteBatch(List<Integer> ids);
    OsVersion query(OsVersion osVersion);
    List<OsVersion> queryAll(OsVersion osVersion);
    List<OsVersion> queryBatch(OsVersion osVersion);
    int queryTotal(OsVersion osVersion);

}
