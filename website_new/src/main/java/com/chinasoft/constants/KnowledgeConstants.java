/*
 *Copyright(c) 2021 Shenzhen Kaihong Digital Industry DevelopmentCo.,Ltd
 *Licensed under the Apache License,Version2.0(the"License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.constants;

public class KnowledgeConstants {
    /**
     * 跳转第一个界面
     */
    public final static String PAGE_STATUS_FIRST = "1";
    /**
     * 跳转第二个界面
     */
    public final static String PAGE_STATUS_SECOND = "2";
    /**
     *  跳转第三个界面
     */
    public final static String PAGE_STATUS_THRID = "3";
    /**
     * 跳转第四个界面
     */
    public final static String PAGE_STATUS_FOURTH = "4";
    /**
     * 跳转第五个界面
     */
    public final static String PAGE_STATUS_FIFTH = "5";

    public static class Status {
        /**
         * 待申请
         */
        public final static int APPLY  = 1;
        /**
         * 审核中
         */
        public final static int REVIEWING = 2;
        /**
         * 已认领
         */
        public final static int CLAIMED  = 3;
        /**
         * 未过审
         */
        public final static int UNAPPROVED  = 4;
    }

}
