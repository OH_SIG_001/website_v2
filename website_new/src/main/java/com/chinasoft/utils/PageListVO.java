/*
 *Copyright(c) 2021 Shenzhen Kaihong Digital Industry DevelopmentCo.,Ltd
 *Licensed under the Apache License,Version2.0(the"License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.utils;

/**
 * 
 * @author Administrator
 *
 * @param <T>
 */
public class PageListVO<T> {
	public  static final Integer SUCCESS = 0;
	public  static final Integer FAIL = 1;

	// 0成功 1失败
    private Integer code;

    //返回信息
    private String msg;

    //页大小
    private Integer pageSize; 
    
    //页码
    private Integer pageNum; 
    
    //总页数
    private Integer totalPage; 
    //总记录
    private Integer totalNum; 
 
    private T data;

    public PageListVO() {
		this.code = SUCCESS;
		this.msg = "成功";
    }


	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getPageNum() {
		return pageNum;
	}

	public void setPageNum(Integer pageNum) {
		this.pageNum = pageNum;
	}

	public Integer getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
	
	public void initPages (Integer totalNum){
		this.totalNum = totalNum;
		if(totalNum ==0)
		{
			
			this.totalPage =0;
		}
		else {
			
			this.totalPage = totalNum % this.pageSize ==0 ? totalNum /  this.pageSize :  (totalNum / pageSize )+1;
		}
		
	}


	public Integer getTotalNum() {
		return totalNum;
	}


	public void setTotalNum(Integer totalNum) {
		this.totalNum = totalNum;
	}



}
